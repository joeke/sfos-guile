#+title: Build script and RPM .spec files for building GNU Guile for SailfishOS

* GNU Guile for SailfishOS
This repo contains .spec files and a build script for building RPM packages for GNU Guile and its dependencies.

The build script assumes that you have [[https://docs.sailfishos.org/Develop/Apps/#sfdk-command-line-tool][sfdk]] installed.
It will fetch the source code for Guile and its dependencies from URLs hardcoded in the build script, which will be put in ~./source-files~. Then, it will compile the downloaded code for a specific Sailfish version and processor architecture, which can be specified using variables at the top of the script. The output RPMs will be put in ~./output-rpms~.


* Acknowledgement
Practically all of the work that went into making these .spec files was done by Fedora contributors.
Tomi Leppänen adapted them for use on SailfishOS in his [[https://github.com/Tomin1/patience-deck][Patience Deck]] application.

A big thank you goes to them.

The original sources for these files are as follows:

- Sailfish adaptations for Patience Deck: https://github.com/Tomin1/patience-deck/tree/master/rpm/misc
- Fedora's guile22 package spec: https://src.fedoraproject.org/rpms/guile22/tree/rawhide
- Fedora's gc package spec: https://src.fedoraproject.org/rpms/gc/tree/rawhide
- Fedora's libunistring package spec: https://src.fedoraproject.org/rpms/libunistring/tree/rawhide
