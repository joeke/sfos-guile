#!/bin/sh

# Change this if sfdk isn't in your $PATH
sfdk_binary=sfdk

# Change this to the desired SDK version
target_sdk_version=4.5.0.18

# Change this to the desired processor architecture (i486, aarch64, etc.)
target_sdk_arch=aarch64



# Create directories for the built RPMs
mkdir ./rpm-output
# and the 'input' source files
mkdir ./source-files



# Add the specified SDK as a target
$sfdk_binary config --push target SailfishOS-$target_sdk_version-$target_sdk_arch

# Tell sfdk to put built RPMs in the rpm-output directory
$sfdk_binary config --push output-prefix "$PWD/rpm-output"

# Initialize the current directory for building with sfdk
$sfdk_binary -c target=SailfishOS-$target_sdk_version-$target_sdk_arch build-init 



# Download the source in the source-files directory

cd ./source-files

wget https://github.com/ivmai/bdwgc/releases/download/v8.2.2/gc-8.2.2.tar.gz
tar xzf ./gc-8.2.2.tar.gz
rm ./gc-8.2.2.tar.gz

wget https://ftp.gnu.org/gnu/libunistring/libunistring-1.1.tar.xz
tar xJf ./libunistring-1.1.tar.xz
rm ./libunistring-1.1.tar.xz

wget https://ftp.gnu.org/pub/gnu/guile/guile-3.0.9.tar.gz
tar xzf guile-3.0.9.tar.gz
rm ./guile-3.0.9.tar.gz

cd ..



# Building

build_using_sfdk () {
    orig_directory=$PWD
    cd $2
    $sfdk_binary -c target=SailfishOS-$target_sdk_version-$target_sdk_arch --specfile $orig_directory/$1.spec build
    cd $orig_directory
}

build_using_sfdk "gc" "./source-files/gc-8.2.2"

build_using_sfdk "libunistring" "./source-files/libunistring-1.1"

build_using_sfdk "guile" "./source-files/guile-3.0.9"



echo "Done!"
